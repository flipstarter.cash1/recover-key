# recover-key
I hope you don't need to use , but if you are here likely you get into trouble ,and would like to restore the private key from your backup mnemonic , so 
follow the following steps

## Access private key

Ask your self ,do I have access to bifrost? is my bifrost running? if the answer is yes , you can just export the private key from bifrost.

If you don't have access to bifrost , do you have access to `thor-damon` ? If yes , then you can shell into thor-daemon and export your private key there.

If you don't have access to `thor-daemon` and `bifrost` , then you will need to get a `thorcli` binary here

### how to shell into bifrost?
```
kubectl exec  -it deploy/bifrost -n thornode -- sh
```

###  how to shell into thor-daemon?
```shell
kubectl exec  -it deploy/thor-daemon -n thornode -- sh
```

### how to restore thor key from mnemonic?
Here we assume you already follow the steps above , and have a valid thorcli binary
```bash
thorcli keys add recovered-key --recover
```

### Export the private key
run the following command to export the private key , you will be asked for password
```
thorcli keys export recovered-key
```
save the output above to a file , it should be something like 

```
-----BEGIN TENDERMINT PRIVATE KEY-----
type: secp256k1
kdf: bcrypt
salt: 6DC3EB7362C136AC8C2771691D190401

HmI1Xoqcf8j7hN0sIOTROsV6H8UN+I7io7ZebzfN8E+zDWOUNYcDXZkNHaKPvtIn
59ikaWUYlSGsu+vMt4c6xP0z+zu7GU8vsYCMip4=
=4hwI
-----END TENDERMINT PRIVATE KEY-----
```

Save  this file in a safe place , let's call it `keys.txt`

## Get binance address , and binance key store file

```
# download recovery tool
wget https://gitlab.com/heimdallthor/recover-key/-/raw/master/release/recover-key.linux
mv recover-key.linux recover-key
chmod +x recover-key
recover-key --file keys.txt --output keystore.key
```
This will ask you the password which you put to export the private key. and it will print out something like 

```shell
hex encoded private key: 804c39a3ec1f28dea89f41b381b9aa7c07b225d0ba956a5499bcd089XXXXXXXX
BNB address: bnb1kswxharhdsfagm38r863jggvqk9jy7u8XXXXXX
```

In the meantime , there is file call `keystore.key` will be created at the same folder you run the command

go to [binance block explorer](https://explorer.binance.org/) , and use the BNB address , you should be able to inspect how much coins in the accounts

go to [binance wallet](https://www.binance.org/en/unlock) , and login with the keystore file (keystore.key) , make sure you have full access of it

# how to return yggdrasil fund manually
There is a script available to help you return yggdrasil fund back to asgard manually

https://repl.it/@heimdallthor/YggdrasilRescue#index.js

1. Please fork the above js file.

2. update line 5, and put in your hex encoded private key ("804c39a3ec1f28dea89f41b381b9aa7c07b225d0ba956a5499bcd089XXXXXXXX")

3. Run it , it is dry-run by default, it should output the coins that it will return.

## How to find out my node pub key

```shell
curl --location --request GET 'http://{node ip address}:1317/thorchain/nodeaccount/{node address}'
```
for example

```shell
curl --location --request GET 'http://3.131.125.237:1317/thorchain/nodeaccount/thor1gz0x5svs3ejjc5fwhunyqlml2p8e6xlq9pzfa8'
```
## How to find out the block height to return yggdrasil
After the vault start churning , run the following command, replace {vault_pubkey} with your own node pub key
replace `{node ip address}` with one valid node ip address 
```shell
curl --location --request GET 'http://{node ip address}:1317/thorchain/queue/outbound' | jq -r '.[]|select(.vault_pubkey=="{vault_pubkey}")'
```
Once you confirm all the details , and want to run it to return fund , uncomment line 84,85 and then run the script a

# how to build the tool
```shell
go build
```
